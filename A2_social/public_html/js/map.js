var center = new google.maps.LatLng(60.1708, 24.9375);
var mapOptions = {
    center: center,
    zoom: 12,
    maxZoom: 14,
    mapTypeId :  google.maps.MapTypeId.HYBRID
};
var markers = [];
var map = new google.maps.Map(document.getElementById("map"),mapOptions);
var bounds = null;

var photoStats = {};
if(mapData){

	// Load the Visualization API and the piechart package.
	google.load('visualization', '1.0', {'packages':['corechart']});

	// Set a callback to run when the Google Visualization API is loaded.
	google.setOnLoadCallback(function(){

		var photoStats = {};

		/***** create the markers *****/
		$.each(mapData,function(placeId,place){
			var name 	= place.name,
				country = place.country,
				city 	= place.city,
				lat 	= place.latitude,
				lng 	= place.longitude,
				photoCounter = place.count;

			var photoPosition = new google.maps.LatLng(lat,lng);
			var marker = new google.maps.Marker({
			    position: photoPosition,
			    map: map,
			    title:name
			});
			markers.push(marker);

			if(!bounds)
				bounds = new google.maps.LatLngBounds(photoPosition,photoPosition);
			else
				bounds.extend(photoPosition);

			map.fitBounds(bounds);


			//correct eventual missing data
			if(!country){
				country = "Unknown";
			}
				
			if(!city)
				city = name;


			//add a new state if there's not already
			if(!photoStats[country]){
				photoStats[country] = {
					totalPhotos : 0,
					cities : {},
					markers : [],
					citiesTable : null
				};
			}

			//add a new city if there's not already
			if(!photoStats[country].cities[city]){
				photoStats[country].cities[city] = {
					totalPhotos : 0,
					markers : []
				};
			}

			//link the markers to the country and the city and update the counters
			photoStats[country].totalPhotos += photoCounter;
			photoStats[country].markers.push(marker);

			photoStats[country].cities[city].totalPhotos += photoCounter;
			photoStats[country].cities[city].markers.push(marker);

		});	

		/**** Start preparation for Chart visualization ****/

		var countriesTable = new google.visualization.DataTable();
		countriesTable.addColumn("string","Country");
		countriesTable.addColumn("number","Photos");
		var countryRows = [];

		$.each(photoStats,function(country,countryStat){

			countryRows.push([country,countryStat.totalPhotos]);

			countryStat.citiesTable = new google.visualization.DataTable();
			countryStat.citiesTable.addColumn("string","City");
			countryStat.citiesTable.addColumn("number","Photos");
			var cityRows = [];
			$.each(countryStat.cities,function(city,cityStat){
				cityRows.push([city,cityStat.totalPhotos]);
			});

			countryStat.citiesTable.addRows(cityRows);
		});

		countriesTable.addRows(countryRows);

		//Visualiza chart
		var chart 		= new google.visualization.PieChart(document.getElementById('country-chart'));
		var cityChart 	= new google.visualization.PieChart(document.getElementById('city-chart'));
		var countrySelected = null;

		var citySelectHandler = null,
			cityOverHandler = null,
			countrySelectHandler = null,
			countryOverHandler = null;

		citySelectHandler = google.visualization.events.addListener(cityChart, 'select', function(){
			var selectedCity = cityChart.getSelection()[0];
			if (selectedCity){
				var city = photoStats[countrySelected].citiesTable.getValue(selectedCity.row, 0);
			    showMarkers(photoStats[countrySelected].cities[city].markers);
			    setBounds(photoStats[countrySelected].cities[city].markers);
			}
			else{
				showMarkers(photoStats[countrySelected].markers);
			    setBounds(photoStats[countrySelected].markers);
			}
		});

		countrySelectHandler = google.visualization.events.addListener(chart, 'select', function(){
			// google.visualization.events.removeListener(citySelectHandler);
   //      	google.visualization.events.removeListener(cityOverHandler);

			var selectedCountry = chart.getSelection()[0];
	        if (selectedCountry) {
		        var country = countriesTable.getValue(selectedCountry.row, 0);
		        console.log("selected "+country);
		        var table = photoStats[country].citiesTable;
		        cityChart.draw(table,{'title':'Photos distribution in '+country});

		        showMarkers(photoStats[country].markers);
			    setBounds(photoStats[country].markers);
			    countrySelected = country;
	        }
	        else{
	        	showAllMarkers();
				resetBounds();
	        }
		}); 
        
        chart.draw(countriesTable,{'title':'Photos distribution by Country'});

 
	});
}

function setBounds(markersArray){
	var center = markersArray[0].getPosition()
	var newBounds = new google.maps.LatLngBounds(center,center);

	for(var i=1;i<markersArray.length;i++)
		newBounds.extend(markersArray[i].getPosition());

	map.fitBounds(newBounds);
}

function resetBounds(){
	map.fitBounds(bounds);
}

function showMarkers(markersArray){
	hideAllMarkers();
	for (var i = 0; i < markersArray.length; i++) 
		markersArray[i].setMap(map);
}

function showAllMarkers(){
	showMarkers(markers);
}

function hideAllMarkers(){
	for (var i = 0; i < markers.length; i++) 
		markers[i].setMap(null);
}