var input= document.getElementById('location');
var autocomplete = new google.maps.places.Autocomplete(input);

google.maps.event.addListener(autocomplete, 'place_changed', function() {

    var place = autocomplete.getPlace();
    if (!place.geometry) {
      return;
    }

    var city = "";
    var country = "";

    for(var i=0;i<place.address_components.length;i++){
    	var component = place.address_components[i];
    	if($.inArray("locality",component.types) != -1 || $.inArray("administrative_area_level_3",component.types) != -1)
    		city = component.long_name;
    	else if($.inArray("country",component.types) != -1)
    		country = component.long_name;
    }

    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();

    $("#lat").attr("value",lat);
    $("#lng").attr("value",lng);
    $("#city").attr("value",city);
    $("#country").attr("value",country);

});

/*** Prevents form to submit without informations ***/

$('#submit').click(function(e) {
	var file = $("#user_file");
	var location = $("#location");
	var lat = $("#lat");
	var lng = $("#lng");
	$(".form-group").removeClass("has-error");

	if(!file.val()){
		file.parents(".form-group").addClass("has-error");
		e.preventDefault(); 
	}
	else if(!location.val() || !lat.val() || !lng.val()){
		location.parents(".form-group").addClass("has-error");
		e.preventDefault(); 
	}
 
});  
