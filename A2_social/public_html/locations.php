<?php

	require_once (__DIR__."/../server/FBUtil.php");
	$FB = new FBUtil();

	$jsVar = null;
	if($FB->isLogged()){
		$jsVar = json_encode(json_encode($FB->getPhotoLocations()));
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>App</title>

    <!-- FB SDK -->
  <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>
  
  <!-- Twitter script -->
  <script>
  !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
  </script>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/photoviewer.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <nav class="navbar navbar-default navbar-inverse" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $FB->serverAddress ?>">The Photo Location Viewer</a>
        </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="main-nav">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo $FB->serverAddress ?>/photouploader.php">Photo Uploader</a></li>
            <li><a href="<?php echo $FB->serverAddress ?>/locations.php">Photo Viewer</a></li>
          </ul>
          <p class="navbar-text navbar-right">
               <?php 
                echo $FB->getLogLink();
               ?>            
          </p>
        </div><!-- /.navbar-collapse -->
      </div>
    </nav>

    <div class="container">

    </div><!-- /.container -->

    <div class="container-fluid">
      <div class="row">
      	<div class="col-sm-8 blog-main text-center">
          <div id="map"></div>
        </div>
        <div class="col-sm-4">
          <?php
            if(!$FB->isLogged()){
          ?>
            <div class="alert alert-danger">
              Seems that you are not logged. Please <strong>login</strong> to see your and your friend's photo locations.
            </div>
          <?php
            }
            else{
              ?>
              <p>Click on a slice to get closer. Click again to clear the zoom.</p>    
              <?php
            }
          ?>
          <div class="row">
            <div class="col-sm-12 chart" id="country-chart"></div>
          </div>
          <div class="row">
            <div class="col-sm-12 chart" id="city-chart"></div>
          </div>
        </div>
      </div>
    </div> <!-- /. container-fluid -->

    <div class="blog-footer">
      <p>&copy; 2014 - All rights reserved, all images, graphics, artwork &copy; 2013 </p>
      <p>Se&ntilde;or Barraja and Mr Cardin unless otherwise noted and may not be used without his permission.</p>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://www.google.com/jsapi"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="js/bootstrap.min.js"></script>
	
	  <script> var mapData = JSON.parse(<?php echo ($jsVar ? $jsVar : 'null') ?>); </script> 
	  <script src="js/map.js"></script>
  </body>
</html>
