<?php
  require_once (__DIR__."/../server/FBUtil.php");
  $FB = new FBUtil();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>App</title>

    <!-- FB SDK -->
  <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>
  
  <!-- Twitter script -->
  <script>
  !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
  </script>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/photoviewer.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <nav class="navbar navbar-default navbar-inverse" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $FB->serverAddress ?>">The Photo Location Viewer</a>
        </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="main-nav">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo $FB->serverAddress ?>/photouploader.php">Photo Uploader</a></li>
            <li><a href="<?php echo $FB->serverAddress ?>/locations.php">Photo Viewer</a></li>
          </ul>
          <p class="navbar-text navbar-right">
               <?php 
                echo $FB->getLogLink();
               ?>            
          </p>
        </div><!-- /.navbar-collapse -->
      </div>
    </nav>

    <div class="container">

      
      <div class="blog-header">
        <h1 class="blog-title">Our Wonderful App</h1>
        <div class="lead blog-description">
          <a href="https://twitter.com/share" class="twitter-share-button" data-text="Nice!" data-hashtags="OurPhotoViewer">Tweet</a>
          <div class="fb-like" data-href="<?php echo $FB->serverAddress ?>/" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
          </div>
      </div>


      <div class="row">
        <div class="col-sm-8 blog-main">

          <div class="blog-post">
            <h2 class="blog-post-title">Something interesting</h2>
            <p class="blog-post-meta">January 1, 2014 by <a href="#">Sech</a></p>

            <p>This blog post shows a few different types of content that's supported and styled with Bootstrap. Basic typography, images, and code are all supported.</p>
            <hr>
            <p>Cum sociis natoque penatibus et magnis <a href="#">dis parturient montes</a>, nascetur ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.</p>
          </div>
          <div class="fb-facepile" data-href="<?php echo $FB->serverAddress ?>/" data-max-rows="1" data-colorscheme="light" data-size="medium" data-show-count="true"></div>
          <div class="fb-comments" data-href="<?php echo $FB->serverAddress ?>/" data-numposts="5" data-colorscheme="light"></div>
      
        </div>

        <div class="col-sm-4 ">
          <div class="sidebar-module sidebar-module-inset">
            <h4>Our Location</h4>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7934.56934187631!2d24.82751899999999!3d60.18665213432983!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x468df5ecd13f9ed1%3A0x4240e7a8e62add5a!2sUniversit%C3%A0+Aalto!5e0!3m2!1sit!2sfi!4v1393253431429" frameborder="0" style="border:0"></iframe>
          </div>
          <div class="sidebar-module">
            <h4>Tweets</h4>
            <a class="twitter-timeline" href="https://twitter.com/SechCardin" data-widget-id="437958509494157312">Tweets di @SechCardin</a>
          </div>
          
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

    <div class="blog-footer">
      <p>&copy; 2014 - All rights reserved, all images, graphics, artwork &copy; 2013 </p>
      <p>Se&ntilde;or Barraja and Mr Cardin unless otherwise noted and may not be used without his permission.</p>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>