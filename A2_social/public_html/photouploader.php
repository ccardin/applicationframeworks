<?php
  
  require_once (__DIR__."/../server/FBUtil.php");
  define("UPLOAD_DIR", "../server/uploads/");

	$FB = new FBUtil();
  $error = null;
  $uploadOk = null;

  if($FB->isLogged() && isset($_FILES['user_file']) && isset($_POST['location'])){

      $file = $_FILES['user_file'];
      
      if($file['error'] == UPLOAD_ERR_OK and is_uploaded_file($file['tmp_name'])){

            move_uploaded_file($file['tmp_name'], UPLOAD_DIR.$file['name']);
          
            $photoInfo = array(
              "path" => UPLOAD_DIR.$file['name'],
              "type" => $file['type'],
              "message" => isset($_POST['message']) ? $_POST['message'] : "",
              "address" => $_POST['location'],
              "lat" => $_POST['latitude'],
              "lng" => $_POST['longitude'],
              "city" => $_POST['city'],
              "country" => $_POST['country']
            );            
          

            $uploadOk = $FB->uploadPhoto($photoInfo);
            if($uploadOk !== true)
              $error = $uploadOk;
      }
      else{
        switch($file['error']){
          case UPLOAD_ERR_INI_SIZE:
            $error = "The file sent exceeds the maximum size allowed by the server";
          break;
          case UPLOAD_ERR_FORM_SIZE:
            $error = "The file sent exceeds the maximum size allowed by the form";
          break;
          case UPLOAD_ERR_PARTIAL:
            $error = "File not received correctly";
          break;
          case UPLOAD_ERR_NO_FILE:
            $error = "No file sent";
          break;
          case UPLOAD_ERR_NO_TMP_DIR:
            $error = "Error while saving the file in the server";
          break;
        }

        $error = "<strong>Server Error!</strong> ".$error;
      }
	}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>App</title>

    <!-- FB SDK -->
  <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>
  
  <!-- Twitter script -->
  <script>
  !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
  </script>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/photoviewer.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <nav class="navbar navbar-default navbar-inverse" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $FB->serverAddress ?>">The Photo Location Viewer</a>
        </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="main-nav">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo $FB->serverAddress ?>/photouploader.php">Photo Uploader</a></li>
            <li><a href="<?php echo $FB->serverAddress ?>/locations.php">Photo Viewer</a></li>
          </ul>
          <p class="navbar-text navbar-right">
               <?php 
                echo $FB->getLogLink();
               ?>            
          </p>
        </div><!-- /.navbar-collapse -->
      </div>
    </nav>

    <div class="container">

      
    <div class="blog-header">
    	<h1 class="blog-title">Upload a photo!</h1>
        <div class="lead blog-description">
	        <a href="https://twitter.com/share" class="twitter-share-button" data-text="Nice!" data-hashtags="OurPhotoViewer">Tweet</a>
	        <div class="fb-like" data-href="<?php echo $FB->serverAddress ?>/" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
        </div>
    </div>


    <?php
      if(!$FB->isLogged())
        $error = "Seems that you are not logged. Please <strong>login</strong> to upload a photo in your profile!";

      if($error)
        echo "<div class='alert alert-danger'>$error</div>";
      else if($uploadOk === true)
        echo "<div class='alert alert-success'><strong>Nice!</strong> Your photo has been uploaded correctly!</div>";
      ?>
    <div class="row">
      <div class="col-sm-10 col-sm-offset-2">
        <p><strong>The fields marked with * are mandatory</strong></p>
      </div>
    </div>
        
        <form action="photouploader.php" method="POST" enctype="multipart/form-data" class="form-horizontal" role="form">
          <fieldset <?php if(!$FB->isLogged()) echo "disabled" ?>>
            <div class="form-group">
              <label for="user_file" class="col-sm-2 control-label">File *</label>
              <div class="col-sm-10">
                <input type="file" class="form-control" name="user_file" id="user_file" placeholder="Select a file to upload">
              </div>
            </div>
            <div class="form-group">
              <label for="location" class="col-sm-2 control-label">Location *</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="location" id="location" placeholder="Enter an address or a place">
                <input type="hidden" id="lat" name="latitude" value="">
                <input type="hidden" id="lng" name="longitude" value="">
                <input type="hidden" id="city" name="city" value="">
                <input type="hidden" id="country" name="country" value="">
              </div>
            </div>
            <div class="form-group">
              <label for="message" class="col-sm-2 control-label">Your message</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="message" id="message" rows="3" placeholder="Your message..."></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" id="submit" class="btn btn-primary">Upload!</button>
              </div>
            </div>
          </fieldset>
        </form>      
    </div><!-- /.container -->

    <div class="blog-footer">
      <p>&copy; 2014 - All rights reserved, all images, graphics, artwork &copy; 2013 </p>
      <p>Se&ntilde;or Barraja and Mr Cardin unless otherwise noted and may not be used without his permission.</p>
    </div>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/uploader.js"></script>

  </body>
</html>
