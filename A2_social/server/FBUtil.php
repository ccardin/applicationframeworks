<?php
	
class FBUtil{



	private $user 			= null;
	private $facebook 		= null;
	public  $serverAddress 	= null;

	public function __construct($token = null) {
    	require_once(__DIR__.'/config.php');

    	$this->serverAddress = $serverAddress;
    	$this->facebook = getfacebook();

        if($token!=null)
            $this->facebook->setAccessToken($token);

        if($this->facebook->getUser()){
            $this->user = $this->facebook->api('/me','GET');
        }

    }

    public function isLogged(){
        if(isset($this->user))
            return true;
        return false;
    }

    public function getLogLink(){

    	//user logged in. return logout link
    	if(isset($this->user)){
    		$logout_url = $this->facebook->getLogoutUrl( array(
			'next' => $this->serverAddress . '/logout.php'  // Logout URL full path
			));
			return "Hello ". $this->user['name'] .", <a href='$logout_url'>Logout</a>";
    	}
    	//user not logged. Return login link
    	else{
    		$login_url = $this->facebook->getLoginUrl(array(
			  'scope' => 'basic_info, user_photos, friends_photos, user_location, friends_location',
              'redirect_uri' => $this->serverAddress."/"
			));
			return "<a href='$login_url'>Login with Facebook</a>";	
    	}
    }

    public function uploadPhoto($photoInfo){
	   try{
            $location_info = $this->facebook->api('/search', 'GET', array(
                'q' => $photoInfo["address"],
                'type' => "place"
                )
            );

            //take the first location found (first by relevance)
            if(count($location_info) == 0 || !isset($location_info["data"][0]) || !isset($location_info["data"][0]["id"]))
                return "Facebook cannot locate the address ".$photoInfo["address"];
            $placeId = $location_info["data"][0]["id"];


            $url = "https://graph.facebook.com/me/photos";
            $fields = array(
            	'access_token' => $this->facebook->getAccessToken(),
            	'source' => "@".$photoInfo["path"],
            	'message' => $photoInfo["message"],
            	'place' => $placeId
            );


            $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_VERBOSE, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields); 
                $result = json_decode(curl_exec($ch),true);

            if($result && isset($result["id"]))
               return true;
            else if($result && isset($result["error"]))
               return "Facebook says: ".$result["error"]["message"];
            else 
               return "For some reason the photo was not uploaded.. [".json_encode($result)."]";

            /** /
            //upload the photo!
            $ret_obj = $this->facebook->api('/me/photos', 'POST', array(
                //'source' => new CURLFile($photoInfo["path"], $photoInfo["type"]),
                'source' => $data,
                'message' => $photoInfo["message"],
                'place' => $placeId
                )
            );
            /**/
            unlink($photoInfo["path"]);
        }
        catch(FacebookApiException $e){
            return $e->getMessage();
        } 
    }

    public function getPhotoLocations(){
        $locationData = $this->queryLocations();
        return $this->formatLocations($locationData);
    }

    public function getPhotoInRadius($lat,$lng,$radius){
        $locationData = $this->queryLocations();
        $locationData = $this->formatLocations($locationData);

        return $this->filterByRadius($locationData,$lat,$lng,$radius);
    }

    private function queryLocations(){
        $myLocations  = "SELECT page_id, name,longitude, latitude from place where page_id in (select place_id from photo where owner = me() and place_id)";
        $friendsLocations = "SELECT page_id, name,longitude, latitude from place where page_id in (select place_id from photo where place_id and owner in (select uid2 from friend where uid1 = me()))";
        $meAndFriends  = "SELECT uid from user where uid = me() or uid in (select uid2 from friend where uid1 = me())";

        $fql = array(
            "photoPlaces"   => "SELECT place_id from photo where place_id and owner in ($meAndFriends)",
            "allLocations"  => "SELECT page_id, name from place where page_id in (select place_id from #photoPlaces)",
            "locationInfo"  => "SELECT page_id, location from page where page_id in (select page_id from #allLocations)",
        );
        

        $queryresults = $this->facebook->api( array(
            'method' => 'fql.multiquery',
            'queries' => $fql,
        ));

        return $queryresults;
    }

    private function formatLocations($locations){
        $places = array();

        $allPhotoPlaces = $locations[0]["fql_result_set"];
        $allLocations = $locations[1]["fql_result_set"];
        $locationInfo = $locations[2]["fql_result_set"];
   
        $size = count($allLocations);
        for($i=0; $i<$size;$i++){
            $id = $allLocations[$i]["page_id"];

            $places[$id] = array(
                "count"     => 0,
                "name"      => $allLocations[$i]["name"],
                "latitude"  => $locationInfo[$i]["location"]["latitude"],
                "longitude" => $locationInfo[$i]["location"]["longitude"],
                "city"      => $this->findCity($locationInfo[$i]["location"]),
                "country"   => $this->findCountry($locationInfo[$i]["location"])
            );
        }

        $size = count($allPhotoPlaces);
        for($i=0; $i<$size;$i++){
            $id = $allPhotoPlaces[$i]["place_id"];
       if(isset($places[$id]))
           $places[$id]["count"] += 1;
        }

        return $places;
    }

    private function filterByRadius($locations,$lat,$lng,$radius){

        $coords = array();
        $counter = 0;
        foreach ($locations as $id => $value){
            $placeLat = $value["latitude"];
            $placeLng = $value["longitude"];    

            if($this->distance($lat,$lng,$placeLat,$placeLng) <= $radius){
                $coords[$counter++] = array("lat" => $placeLat, "lng" => $placeLng);
            }
                //unset($locations[$id]);
        }

        return $coords;
    }

    private function findCity($location){
        if(array_key_exists("city", $location))
            return $location["city"];
        return null;
    }

    private function findCountry($location){
        if(array_key_exists("country", $location))
            return $location["country"];
        return null;
    }

    private function distance($centerLat,$centerLng,$placeLat,$placeLng){
        $lat1 = $centerLat * (pi()/180);
        $lng1 = $centerLng * (pi()/180);
        $lat2 = $placeLat  * (pi()/180);
        $lng2 = $placeLng  * (pi()/180);

        $d = acos(sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos($lng1 - $lng2))*6371;
        return $d;
    }

}
?>
