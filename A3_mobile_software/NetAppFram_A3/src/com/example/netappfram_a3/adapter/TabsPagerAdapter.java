package com.example.netappfram_a3.adapter;

import com.example.netappfram_a3.FirstTaskFragment;
import com.example.netappfram_a3.SecondTaskFragment;
import com.example.netappfram_a3.ThirdTaskFragment;
import com.example.netappfram_a3.FourthTaskFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Top Rated fragment activity
			return new FirstTaskFragment();
		case 1:
			// Games fragment activity
			return new SecondTaskFragment();
		case 2:
			// Movies fragment activity
			return new ThirdTaskFragment();
		case 3:
			// Movies fragment activity
			return new FourthTaskFragment();
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 4;
	}

}