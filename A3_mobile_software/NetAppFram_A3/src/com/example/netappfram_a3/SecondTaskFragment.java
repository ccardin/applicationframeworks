package com.example.netappfram_a3;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Date;
import com.example.netappfram_a3.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SecondTaskFragment extends Fragment {

	private long date = 0;
//final String textSource = "http://sites.google.com/site/androidersite/text.txt";
//final String textSource = "https://playground.cs.hut.fi/t�110.5140/hello.txt";
	 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_second_task, container, false);
		
		new Thread() {

            public void run() {
                String path ="http://txt2html.sourceforge.net/sample.txt";
                URL u = null;
                try {
                    u = new URL(path);
                    HttpURLConnection c = (HttpURLConnection) u.openConnection();
                    c.setRequestMethod("GET");
                    c.connect();

                    InputStream in = c.getInputStream();
                    final ByteArrayOutputStream bo = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    in.read(buffer); // Read from Buffer.
                    bo.write(buffer); // Write Into Buffer.
                    

                    date = c.getLastModified();

                    
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            TextView text = (TextView) getActivity().findViewById(R.id.textView2_1);
                            TextView text2 = (TextView) getActivity().findViewById(R.id.textView2_2);
                            text.setText(bo.toString());
                            if (date == 0)
                            	text2.setText("No last-modified information.");
                            else
                            	text2.setText("Last-Modified: " + new Date(date));
                            try {
                                bo.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }.start();
        
	  return rootView;
	}
}
