package com.example.netappfram_a3;

import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.example.netappfram_a3.adapter.TabsPagerAdapter;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

	public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
	private ViewPager viewPager;		//
	private TabsPagerAdapter mAdapter;	//
	private ActionBar actionBar;		//
	// Tab titles
	private String[] tabs = { "Task_1", "Task_2", "Task_3","Task_4" };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Initilization
		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());

		viewPager.setAdapter(mAdapter);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);		

		// Adding Tabs
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));
		}

		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}
	
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
		// show respected fragment view
		viewPager.setCurrentItem(tab.getPosition());
	}

	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}
	
    /** Called when the user clicks the Send button */
    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);

    }
    
    /** Called when the user clicks the Send URL button */
    public void sendUrl(View view) {
        Intent urlIntent = new Intent(this, DisplayUrlActivity.class);
        EditText editUrlText = (EditText) findViewById(R.id.edit_url);
        String urlMessage = editUrlText.getText().toString();
        urlIntent.putExtra(EXTRA_MESSAGE, urlMessage);
        startActivity(urlIntent);

    }
    
    /*---------------------------------------------------------------------*/
    
	
	public void sendWoeid(View view) {
		EditText woeid = (EditText)findViewById(R.id.edit_woeid);
    	String surWoeid = woeid.getText().toString();
		new DownloadWeatherTask().execute("http://weather.yahooapis.com/forecastrss?w="+surWoeid);
    	//TextView texCount = (TextView)findViewById(R.id.txtCountry);
    	//texCount.setText(surWoeid);		
	}
	
	
	private class DownloadWeatherTask extends AsyncTask<String, Integer, Boolean> {

		StringBuilder sb=null;
		int temp;
		String date;
		String countcit;
		
		@Override
		protected Boolean doInBackground(String... params) {
			try {
				URL weather = new URL(params[0]);
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				factory.setValidating(false);
				factory.setNamespaceAware(true);

				DocumentBuilder builder = factory.newDocumentBuilder();
				// the "parse" method also validates XML, will throw an exception if
				// misformatted
				Document document = builder.parse(new InputSource(weather.openStream()));
				document.getDocumentElement().normalize();
				Element root = document.getDocumentElement();		
				NodeList channels =  root.getElementsByTagName("channel");
				Element channel = (Element) channels.item(0);
				Element items = (Element) channel.getElementsByTagName("item").item(0);
				Element itemweather = (Element) items.getElementsByTagName("yweather:condition").item(0);
				Element locations = (Element) channel.getElementsByTagName("yweather:location").item(0);
				date = itemweather.getAttribute("date");
			    countcit = locations.getAttribute("city")+ ", "+  locations.getAttribute("country");
			    temp = Integer.parseInt(itemweather.getAttribute("temp"));
				   
			    
				
			}catch (Exception e)
			{
				e.printStackTrace();
			}
			return Boolean.TRUE;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			TextView texCount = (TextView)findViewById(R.id.txtCountry);
			TextView texDate = (TextView)findViewById(R.id.txtDate);
			TextView texTemp = (TextView)findViewById(R.id.txtTemp);
			texCount.setText(countcit);
			texDate.setText(date);
			texTemp.setText(Integer.toString(temp)+" F");
		}
		
	};

    
    
    
}
