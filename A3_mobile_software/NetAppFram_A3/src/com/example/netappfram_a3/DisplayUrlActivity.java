package com.example.netappfram_a3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

public class DisplayUrlActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_url);

		
		// Get the message from the intent
        Intent intentUrl = getIntent();
        String messageUrl = intentUrl.getStringExtra(MainActivity.EXTRA_MESSAGE);
        
        WebView webPage = new WebView(this);
        webPage.loadUrl(messageUrl);
        setContentView(webPage);
        
	}

}
