package com.example.naf_2_2;

import android.app.Activity;
import android.util.Log;

import com.sromku.simple.fb.Permission.Type;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.entities.Profile.Properties;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

public class FBUtil {
	
	protected static final String TAG = "simple";
	private SimpleFacebook fb = null;
	private Profile userProfile = null;
	
	FBUtil(Activity act){
		fb = SimpleFacebook.getInstance(act);
	}
	
	public String getAccessToken(){
		if(fb!= null && fb.isLogin())
			return fb.getSession().getAccessToken();
		return null;
	}
	
	public String getUsername(){
		if(userProfile != null)
			return userProfile.getName();
		return null;
	}
	
	public boolean isLogin(){
		if(fb!= null)
			return fb.isLogin();
		return false;
	}
	
	public void login(final OnLoginListener onLoginListener){
		Log.i("fb","Login called");
		
		if(fb!= null){
			fb.login(new OnLoginListener(){
				public void onLogin() {
					Profile.Properties properties = new Profile.Properties.Builder()
					    .add(Properties.ID)
					    .add(Properties.NAME)
					    .build();
					
					OnProfileListener profileCallback = new OnProfileListener() {         
					    @Override
					    public void onComplete(Profile profile) {
					    	Log.i("fb","Got Profile");
					    	userProfile = profile;
					    	onLoginListener.onLogin();
					    }
					};
					fb.getProfile(properties, profileCallback);
				}
				public void onThinking() {}
				public void onException(Throwable throwable) {}
				public void onFail(String reason) {}
				public void onNotAcceptingPermissions(Type type) {}
				
			});
		}
		else
			onLoginListener.onFail("FBUtil class not initialized correctly.");
	}
	
	public void logout(OnLogoutListener onLogout){
		userProfile = null;
		fb.logout(onLogout);
	}

	public void fetchProfile(final OnLoginListener loginCallback) {
		Profile.Properties properties = new Profile.Properties.Builder()
	    .add(Properties.ID)
	    .add(Properties.NAME)
	    .build();
	
	OnProfileListener profileCallback = new OnProfileListener() {         
	    @Override
	    public void onComplete(Profile profile) {
	    	Log.i("fb","Got Profile");
	    	userProfile = profile;
	    	loginCallback.onLogin();
	    }
	};
	
	fb.getProfile(properties, profileCallback);
		
	}


}
