package com.example.naf_2_2;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JsonApiParser {
	
	public static interface OnParseDone{
		void parseFinished(double[] coordinates, String err);
	}
	String url = null;
	
	JsonApiParser(String requesturl){
		url = requesturl;
		Log.i("json","url "+url);
	}
	
	public void parse(final OnParseDone callback){
		new Thread(new Runnable() {
		    public void run() {
		    	JSONArray jArray = MakeRequest();
				if(jArray == null){
					callback.parseFinished(null,"Error during the request");
					return;
				}
				double[] coords = FormatJson(jArray);
				if(coords == null){
					callback.parseFinished(null,"Error during the data formatting");
					return;
				}
				callback.parseFinished(coords,"");
		    }
		  }).start();
		
	}
	
	private double[] FormatJson(JSONArray jArray){
		double[] coords = new double[jArray.length()*2];
		
		try {
			for (int i=0; i < jArray.length(); i++){
			    
			        JSONObject location = jArray.getJSONObject(i);
			        coords[i*2] = location.getDouble("lat");
			        coords[(i*2)+1] = location.getDouble("lng");
			        
			}
			
			return coords;
		} catch (JSONException e) {
			return null;
		}
	}

	private JSONArray MakeRequest(){
		Log.i("json","MakeRequest start "+url);
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet getRequest = new HttpGet(url);
		// Depends on your web service
		getRequest.setHeader("Content-type", "application/json");

		InputStream inputStream = null;
		String result = null;
		try {
			Log.i("json","MakeRequest execute");
		    HttpResponse response = httpclient.execute(getRequest);   
		    Log.i("json","MakeRequest get entity");
		    HttpEntity entity = response.getEntity();
		    
		    Log.i("json","MakeRequest get content");
		    inputStream = entity.getContent();
		    
		    // json is UTF-8 by default
		    Log.i("json","MakeRequest buffer");
		    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
		    StringBuilder sb = new StringBuilder();
		    

		    String line = null;
		    while ((line = reader.readLine()) != null)
		    {
		    	Log.i("json","line "+line);
		        sb.append(line + "\n");
		    }
		    
		    result = sb.toString();
		    
		    Log.i("json","MakeRequest done! "+result);
		    return new JSONArray(result);
		    
		} catch (Exception e) { 
		    Log.e("exception",e.toString());
		    return null;
		}
		finally {
		    try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
		}
	}
}
