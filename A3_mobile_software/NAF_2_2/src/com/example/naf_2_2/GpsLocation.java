package com.example.naf_2_2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

public class GpsLocation {
	

	private LocationListener changeCallback = null;
	private double latitude = 60.1708;
	private double longitude = 24.9375;
	
	GpsLocation(Activity act, LocationListener onLocationChange){
		
		changeCallback = onLocationChange;
		
		
		// Get the location manager
		LocationManager locationManager = (LocationManager) act.getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the location provider
        Criteria criteria = new Criteria();
        //criteria.setAccuracy(Criteria.ACCURACY_COARSE);   //default
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setCostAllowed(false);
        // get the best provider depending on the criteria
        String provider = locationManager.getBestProvider(criteria, false);
        // the last known location of this provider
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null)
        {
        	longitude = location.getLongitude();
    		latitude = location.getLatitude();
        	locationListener.onLocationChanged(location);
        }
        else{
        	// leads to the settings because there is no last known location
        	Intent locationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        	act.startActivity(locationIntent);
        }
        // location updates: at least 1 meter and 200millsecs change
        locationManager.requestLocationUpdates(provider, 200, 1, locationListener);
	}
	
	public double getLat(){
		return latitude;
	}
	
	public double getLng(){
		return longitude;
	}
	
	private final LocationListener locationListener = new LocationListener() {
	    public void onLocationChanged(Location location) {
	        longitude = location.getLongitude();
	        latitude = location.getLatitude();
	        changeCallback.onLocationChanged(location);
	    }

		@Override
		public void onProviderDisabled(String provider) {}

		@Override
		public void onProviderEnabled(String provider) {}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};
	
}
