package com.example.naf_2_2;

//import android.support.v7.app.ActionBarActivity;
//import android.support.v7.app.ActionBar;
//import android.support.v4.app.Fragment;


import java.util.ArrayList;

import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;

import android.util.Log;
import android.view.View; 
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

public class MainActivity extends Activity {

	public static final String ApiAddress = "http://group10.naf.cs.hut.fi/api/locations.php";
	private TextView latitude;
	private TextView longitude;
	private TextView usernameText;
	private EditText radiusText;
	private Button sendButton;
	private Button loginButton;
	private GpsLocation gps;
	private FBUtil fb;
  
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		radiusText = (EditText) findViewById(R.id.editRange);
		latitude = (TextView) findViewById(R.id.latitude_value);
		longitude = (TextView) findViewById(R.id.longitude_value);
		usernameText = (TextView) findViewById(R.id.username_value);
		loginButton = (Button) findViewById(R.id.connect_fb_button);
		sendButton = (Button) findViewById(R.id.send_button);
		
		gps = new GpsLocation(this,locationListener);
		InitFacebook();
        

        sendButton.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				Log.i("json","Onclick sebd button");
				MakeRestRequest();
            }
        });
	}
	
	
	
	
	
	private void MakeRestRequest(){
		Log.i("json","Makerequest");
		String token = fb.getAccessToken();
		double lat = gps.getLat();
		double lng = gps.getLng();
		int radius;
		String radText = radiusText.getText().toString();
		Log.i("json","RadText: "+radText);
		radius = radText.isEmpty() ? 10 : Integer.parseInt(radiusText.getText().toString());
		
		String request = ApiAddress + 
				"?token="+ token +
				"&lat="+ lat +
				"&lng="+ lng +
				"&radius="+ radius;
		
		new JsonApiParser(request).parse(onParseDone);
	}
	
    /** Called when the user clicks the Send button */
    public void displayMap(double[] coordinates) {
    	Intent intent = new Intent(this, DisplayMapActivity.class);
        
        intent.putExtra("coordinates", coordinates);
        
        startActivity(intent);
    }

    private void InitFacebook(){
    	Permission[] permissions = new Permission[] {
				Permission.BASIC_INFO,
				Permission.USER_PHOTOS,
			    Permission.FRIENDS_PHOTOS,
			    Permission.USER_LOCATION,
			    Permission.FRIENDS_LOCATION
			};
		
		SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
		    .setAppId("1403174219945453")
		    .setNamespace("sromkuapp")
		    .setPermissions(permissions)
		    .build();
		
		SimpleFacebook.setConfiguration(configuration);
		SimpleFacebook simplefb = SimpleFacebook.getInstance(this);
		fb = new FBUtil(this);
		
		Log.i("fb","FB is "+ (simplefb == null ? "null" : "valid"));
		if(fb.isLogin())
			fb.fetchProfile(loginCallback);
		
		loginButton.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
            	Log.i("fb","Click Login. IsLogged: "+fb.isLogin()+"\n");
            	if(fb.isLogin())
            		fb.logout(logoutCallback);
            	else
            		fb.login(loginCallback);	
            }
        });
    }
    
    private OnLoginListener loginCallback = new OnLoginListener() {
	    @Override
	    public void onLogin() {
	    	
	    	Log.i("fb","Got Profile");
	    	usernameText.setText(fb.getUsername());
	    	loginButton.setText(R.string.com_facebook_loginview_log_out_button);
	    	tryEnableSendButton();
	    }
	    
	    public void onNotAcceptingPermissions(Permission.Type type) {}
		public void onThinking() {}
		public void onException(Throwable throwable) {}
		public void onFail(String reason) {
			Toast.makeText(MainActivity.this, "Unsuccessful login: "+ reason,Toast.LENGTH_SHORT).show();
		}
	};
	
	private OnLogoutListener logoutCallback = new OnLogoutListener() {
	    @Override
	    public void onLogout() {
	    	Log.i("fb","Executing Logout Callback\n");
	    	usernameText.setText(R.string.not_logged_in);
	    	loginButton.setText(R.string.com_facebook_loginview_log_in_button);
	    	tryEnableSendButton();
	    }
	    
		public void onThinking() {}
		public void onException(Throwable throwable) {}
		public void onFail(String reason) {
			Toast.makeText(MainActivity.this, "Unsuccessful logout: "+ reason,Toast.LENGTH_SHORT).show();
		}
	};
	
	private JsonApiParser.OnParseDone onParseDone = new JsonApiParser.OnParseDone(){

		@Override
		public void parseFinished(double[] coordinates, String err) {
			Log.i("json","Executing JSONParse Callback. Error: "+err);
			displayMap(coordinates);		
		}
	};

	private void tryEnableSendButton() {
		if(fb != null && gps != null){
			
			String token = fb.getAccessToken();
			double lat = gps.getLat();
			double lng = gps.getLng();
			
			if(fb.isLogin() == false || token == null || (lat == 0 && lng == 0) )
				sendButton.setEnabled(false);
			else
				sendButton.setEnabled(true);
		}
	}
	
	private final LocationListener locationListener = new LocationListener() {
	    public void onLocationChanged(Location location) {
	    	Log.i("gps","Activity LocationListener ");
	    	Log.i("gps","Activity location "+location.toString());
	    	if(location != null){
		    	// Initialize the location fields
				latitude.setText(String.valueOf(location.getLatitude()));
				longitude.setText(String.valueOf(location.getLongitude()));
				
				//Toast.makeText(MainActivity.this,  "Location changed!",Toast.LENGTH_SHORT).show();
				tryEnableSendButton();
	    	}
	    }
	    @Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			Toast.makeText(MainActivity.this, provider + "'s status changed to "+status +"!",Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onProviderEnabled(String provider) {
			Toast.makeText(MainActivity.this, "Provider " + provider + " enabled!", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onProviderDisabled(String provider) {
			Toast.makeText(MainActivity.this, "Provider " + provider + " disabled!",Toast.LENGTH_SHORT).show();
		}
	};
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    SimpleFacebook.getInstance().onActivityResult(this, requestCode, resultCode, data); 
	    super.onActivityResult(requestCode, resultCode, data);
	} 

}
