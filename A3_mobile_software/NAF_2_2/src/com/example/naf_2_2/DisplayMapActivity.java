package com.example.naf_2_2;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class DisplayMapActivity extends Activity   {

	private GoogleMap map; 
	private LatLng coordinates;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_map);
		
		Intent intent = getIntent();
		double[] coords = intent.getDoubleArrayExtra("coordinates");
		
		
		LatLngBounds.Builder builder = new LatLngBounds.Builder();
		map =((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		map.clear();
		for(int i=0; i<coords.length/2;i++){
			coordinates=new LatLng(coords[i*2], coords[i*2+1]);
			builder.include(coordinates);
			map.addMarker(new MarkerOptions()
			  .position(coordinates)
			  .title("Hello world1:"));
		}
		
		int width = 200;
		int height = 200;
		int padding = 0;
		
		LatLngBounds bounds = builder.build();
		CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
		map.animateCamera(cu);
		
		 
	}
}

