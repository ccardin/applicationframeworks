In order to install the application, please follow the instructions below:

- Copy all the folders inside the A3_mobile_software folder in your Eclipse workspace.
- In Eclipse, go to File -> Import -> Android -> Existing Android Code to Workspace
- Browse to the A3_mobile_software folder copied before and import all the projects in there (NAF_2_2, NetAppFram_A3) and all the projects inside the dependencies folder.
- In the Eclipse Package explorer, right click on the NetAppFram_A3 project -> properties -> Android and add the project "android-support-v7-appcompat" as dependency.
- In the Eclipse Package explorer, right click on the NAF_2_2 project -> properties -> Android and add the project "android-support-v7-appcompat", "Facebook SDK", "google-play-servicies_lib", "Simple Facebook" as dependencies.

Build the projects and run them in your mobile phone or AVD.

