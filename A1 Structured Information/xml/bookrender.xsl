<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:bs="myBookshelf"
  xmlns:au="myAuthors"
  >


  <!-- Match the root element -->
  <xsl:template match="bs:bookshelf">
      
      <html>
        <head>
          <title>My Bookshelf</title>
        </head>
        <body>
          <table border="1">
            <tr bgcolor="#9acd32">
              <th>Title</th>
              <th>Is Missing?</th>
              <th>Authors</th>
              <th>Language</th>
              <th>Category</th>
              <th>Subcategory</th>
            </tr>
            <xsl:apply-templates select="bs:book"/>
          </table>
        </body>
      </html>
  </xsl:template>

  <!-- Match the book element -->
  <xsl:template match="bs:book">
     <tr>
        <td><xsl:value-of select="bs:title" /></td>
        <td><xsl:value-of select="@isMissing" /></td>
        <td><xsl:apply-templates select="bs:authors"/></td>
        <td><xsl:value-of select="bs:language" /></td>
        <td><xsl:value-of select="bs:category" /></td>
        <td><xsl:value-of select="bs:subCategory" /></td>
      </tr>
  </xsl:template>

  <!-- Match the authors element -->
  <xsl:template match="bs:authors">
    <ol>
      <xsl:for-each select="au:author">
        <li>
          <xsl:value-of select="au:firstName" />
          <xsl:value-of select="au:lastName" />
        </li>
      </xsl:for-each>    
    </ol>
  </xsl:template>


</xsl:stylesheet>