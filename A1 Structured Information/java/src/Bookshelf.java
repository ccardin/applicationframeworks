import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;
 
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

//import com.sun.xml.internal.fastinfoset.stax.events.XMLConstants;




public class Bookshelf {
	public static void main( String[] args)
	{
		System.out.println("----------------------------");
		if(args.length != 2){
			System.out.println("Error: provide 2 parameters. Got "+args.length);
			System.out.println("Usage: Bookshelf xml-path xsd-path");
			return;
		}
		
		//String xsdPath = "/home/cardinc1/Desktop/applicationframeworks/A1 Structured Information/xsd/book_schema.xsd";
		//String xmlPath = "/home/cardinc1/Desktop/applicationframeworks/A1 Structured Information/xml/bookshelf.xml";
		String xmlPath = args[0];
		String xsdPath = args[1];
		Document doc = LoadXml(xmlPath);
		
		if(doc == null)
			return;

		boolean isValid = ValidateSchema(xmlPath,xsdPath);
		System.out.println("The document is "+ (isValid ? "valid" : "invalid") + "!");
		
		System.out.println("----------------------------\n");
		if(isValid){
			ShowXmlDocument(doc);
		}
		else{
			System.out.println("Please correct all the errors to display the content \n");
		}
		
		
		
	}
	
	public static boolean ValidateSchema(String xmlPath, String xsdPath){
		try {
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			// create a grammar object.
			File xsdFile = new File(xsdPath);
			if(xsdFile.exists()){
				Source[] source = { new StreamSource(xsdFile)};

				Schema schema = schemaFactory.newSchema(source);
				Validator schemaValidator = schema.newValidator();
				  
				// validate xml instance against the grammar.
				schemaValidator.validate(new StreamSource(xmlPath));
				return true;
			}
			else{
				System.out.println("Error: "+xsdPath+" doesn't exists");
				return false;
			}

        } catch (Exception e) {
            System.out.println("Exception: "+e.getMessage());
            return false;
        }
        
    }
	
	public static Document LoadXml(String xmlPath){
		try{
			Document doc = null;
			File xmlFile = new File(xmlPath);
			if(xmlFile.exists()){
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				doc = dBuilder.parse(xmlFile);
			}
			else{
				System.out.println("Error: "+xmlPath+" doesn't exists");
			}
			
			
			return doc;
		}
		catch(Exception e){
			System.out.println("Exception: "+e.getMessage());
            return null;
		}
	}

	public static String GetSchemaPath(Document doc){
		if(doc == null)
			return "";
		String xsdPath = doc.getDocumentElement().getAttribute("xs:schemaLocation");
		try{
			File xsdFile = new File(xsdPath);
			return URLDecoder.decode(xsdFile.getAbsolutePath(),"UTF-8");
		}
		catch(Exception e){
			System.out.println("Exception: "+e.getMessage());
			return "";
		}
	}

	public static void ShowXmlDocument(Document doc){
		
		if(doc == null)
			return;
		
		String title, missing, language, category, authors;
	    try 
	    {
	    	 
		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		doc.getDocumentElement().normalize();
	 
		NodeList nodeList = doc.getElementsByTagName("bs:book");
		System.out.println(nodeList.getLength()+" books in the bookshelf.\n");

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			
			
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				
				title = "Title: "+ element.getElementsByTagName("bs:title").item(0).getTextContent();
				missing = (element.getAttribute("isMissing") == "true" ? "(missing from shelf)" : "");
				language = "Langiage: "+ element.getElementsByTagName("bs:language").item(0).getTextContent();
				category = "Category: "+ element.getElementsByTagName("bs:category").item(0).getTextContent();
				
				NodeList authorsList = element.getElementsByTagName("au:author");
				authors = "Author: ";
				for(int j = 0;j<authorsList.getLength();j++){
					Element author = (Element) authorsList.item(j);
					authors += author.getElementsByTagName("au:firstName").item(0).getTextContent() + " ";
					authors += author.getElementsByTagName("au:lastName").item(0).getTextContent();
					if(j != authorsList.getLength() -1)
						authors += ", ";
				}
				
				System.out.println(title + " " + missing);
				System.out.println(language);
				System.out.println(category);
				System.out.println(authors);
				System.out.println();
			}
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
}
