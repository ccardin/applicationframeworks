For this project the files are organized in 3 folders:

java	: contains the source code of the validator program (Bookshelf.java)
xsd	: contains the two definition schemas (book_schema.xsd and authors_schema.xsd)
xml	: contains an example of valid xml file (bookshelf.xml) and the xslt transformation file (bookrenderer.xsl)



**** To compile the validator program ****

javac Bookshelf.java


**** Validator usage ****

java Bookshelf xml_path xsd_path

The program takes in input the path of the xml file and the path of the xsd file to validate against (absolute or relative to the 
executable's location). Eventual imported schemas will be automatically loaded.
Usage Example:

java Bookshelf ../xml/bookshelf.xml ../xsd/book_schema.xsd



**** XSLT transformation ****

To see the result of the transformation, open the bookshelf.xml file with a browser capable of apply a XSLT transformation offline
(Firefox)

